require File.expand_path("../../test_helper", __FILE__)


class ApplicationHelperTest < ActionView::TestCase
    
test"full title helper" do
    assert_equal full_title, "Ruby on Rails Tutorial Sample App"
    assert_equal full_title("About"), "About | Ruby on Rails Tutorial Sample App"
    
end
end