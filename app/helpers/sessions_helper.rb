module SessionsHelper
    #logs the user in
    def log_in(user)
    session[:user_id] = user.id #creates session cookie temporary cookie
    end
    
    def log_out
          session.delete(:user_id)
    @current_user = nil
        
    end
    
    #returns current user if there is any
    def current_user
      @current_user ||= User.find_by(id: session[:user_id])
    end
    
    #returns whether is a user logged in
    
    def logged_in?
        !current_user.nil?
    end
end