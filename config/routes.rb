Rails.application.routes.draw do
  root 'static_pages#home'
      resources :users
       get 'login', to:'sessions#new'
       post 'login', to: 'sessions#create'
       delete'logout', to: 'sessions#destroy'
  
get 'signup' => 'users#new'
get 'about'=> 'static_pages#about'
get 'help'=> 'static_pages#help'
get 'home'=> 'static_pages#home'
get 'contact' => 'static_pages#contact'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
